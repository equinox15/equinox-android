package com.elevenpaths.equinox.sqlite;

/***
 * @author Íñigo Serrano & Miguel Hernández
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class SQLite extends SQLiteOpenHelper {


    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "users.db";
    private static final String TABLE_USER = "user";
    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String KEY = "key";
    private static final String CREATE_TABLE_USER = "CREATE TABLE " + TABLE_USER + "(" +
            ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            NAME + " TEXT, " +
            KEY + " TEXT)";

    public SQLite(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_USER);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
    }

    public void insertUser(@NonNull User user) {
        SQLiteDatabase db = this.getWritableDatabase();


        String selectQuery = "SELECT  * FROM " + TABLE_USER + " WHERE " + NAME + " = '" + user.getName() + "'";

        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null) {
            c.moveToFirst();
            ContentValues values = new ContentValues();
            values.put(NAME, user.getName());
            values.put(KEY, user.getKey());
            if (c.getCount() == 0) {
                db.insert(TABLE_USER, null, values);
            } else {
                db.update(TABLE_USER, values, ID + " = " + user.getId(), null);
            }
            c.close();
        }
        closeDB();
    }


    public User selectUser(String name) {
        SQLiteDatabase db = this.getWritableDatabase();

        String selectQuery = "SELECT  * FROM " + TABLE_USER + " WHERE " + NAME + " = '" + name + "'";
        Cursor c = db.rawQuery(selectQuery, null);

        User user = null;
        if (c != null) {
            user = new User();
            c.moveToFirst();
            user.setName(c.getString(c.getColumnIndexOrThrow(NAME)));
            user.setId(c.getInt(c.getColumnIndexOrThrow(ID)));
            user.setKey(c.getString(c.getColumnIndexOrThrow(KEY)));
            c.close();
        }
        closeDB();
        return user;
    }

    public void deleteUser(@NonNull User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_USER, ID + "=" + user.getId(), null);
        closeDB();
    }


    public List<String> getAllUsers() {
        List<String> list = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_USER;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null) {
            while (c.moveToNext()) {
                String name = c.getString(c.getColumnIndexOrThrow(NAME));
                list.add(name);
            }
            c.close();
        }
        closeDB();
        return list;
    }

    private void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen()) {
            db.close();
        }
    }
}
