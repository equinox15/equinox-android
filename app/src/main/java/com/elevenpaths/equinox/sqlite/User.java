package com.elevenpaths.equinox.sqlite;

/***
 * @author Íñigo Serrano & Miguel Hernández
 */

public class User {

    private int id;
    private String name;
    private String key;

    public User() {
    }

    public User(String name, String key) {
        this.name = name;
        this.key = key;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
