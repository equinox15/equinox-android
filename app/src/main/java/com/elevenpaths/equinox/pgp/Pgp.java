package com.elevenpaths.equinox.pgp;

/***
 * @author Íñigo Serrano & Miguel Hernández
 */

import android.app.Activity;

import com.elevenpaths.equinox.util.Util;
import com.ning.http.util.Base64;

import org.spongycastle.jce.provider.BouncyCastleProvider;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.crypto.Cipher;

public class Pgp {

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    private static KeyPair keyPair;

    public static KeyPair initKeyPair() {
        try {
            keyPair = KeyPairGenerator.getInstance("RSA").generateKeyPair();
        } catch (NoSuchAlgorithmException e) {
            System.err.println("Algorithm not supported! " + e.getMessage() + "!");
        }
        return keyPair;
    }

    public static String encrypt(String texto, String clave) throws GeneralSecurityException, IOException {
        Cipher cifrador = Cipher.getInstance("RSA");
        PublicKey pkey = Util.loadPublicKey(clave);
        cifrador.init(Cipher.ENCRYPT_MODE, pkey);

        byte[] t = texto.getBytes();
        List<byte[]> aux2 = new ArrayList<>();
        List<byte[]> BlocksCode = divideArray(t, 32);
        byte[] aux;
        for (byte[] bloque : BlocksCode) {
            aux = cifrador.doFinal(bloque);
            aux2.add(aux);
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        for (byte[] a : aux2) {
            outputStream.write(a);
        }
        byte c[] = outputStream.toByteArray();
        return Base64.encode(c);
    }

    public static String decrypt(Activity app, String texto) throws GeneralSecurityException, IOException {
        Cipher cifrador = Cipher.getInstance("RSA");
        String privada = Util.leerPrivada(app);
        PrivateKey pkey = Util.loadPrivateKey(privada);
        cifrador.init(Cipher.DECRYPT_MODE, pkey);

        byte[] ciphertextBytes = Base64.decode(texto);
        byte[] decryptedBytes = cifrador.doFinal(ciphertextBytes);
        return new String(decryptedBytes);
    }

    private static List<byte[]> divideArray(byte[] source, int chunksize) {
        List<byte[]> result = new ArrayList<>();
        int start = 0;
        while (start < source.length) {
            int end = Math.min(source.length, start + chunksize);
            result.add(Arrays.copyOfRange(source, start, end));
            start += chunksize;
        }

        return result;
    }
}
