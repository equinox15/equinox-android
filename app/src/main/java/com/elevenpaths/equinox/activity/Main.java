package com.elevenpaths.equinox.activity;

/***
 * @author Íñigo Serrano & Miguel Hernández
 */

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.elevenpaths.equinox.R;
import com.elevenpaths.equinox.pgp.Pgp;
import com.elevenpaths.equinox.sqlite.SQLite;
import com.elevenpaths.equinox.sqlite.User;
import com.elevenpaths.equinox.util.Util;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.List;

public class Main extends AppCompatActivity implements OnClickListener {

    private SQLite sqlite;
    private String texto;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);

        sqlite = new SQLite(getApplicationContext());

        Button cifrar = (Button) findViewById(R.id.cifrar);
        Button descifrar = (Button) findViewById(R.id.descifrar);
        Button keys = (Button) findViewById(R.id.keys);
        Button contactos = (Button) findViewById(R.id.contact);
        cifrar.setOnClickListener(this);
        descifrar.setOnClickListener(this);
        keys.setOnClickListener(this);
        contactos.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        EditText editText = (EditText) findViewById(R.id.editText);
        texto = editText.getText().toString().trim();
        switch (v.getId()) {
            case R.id.cifrar:
                if (texto.trim().length() > 0) {
                    Toast.makeText(getApplicationContext(), "Vas a cifrar: " + texto, Toast.LENGTH_LONG).show();
                    mostrarUser();
                }
                break;
            case R.id.descifrar:
                if (texto.trim().length() > 0) {
                    Toast.makeText(getApplicationContext(), "Vas a descifrar: " + texto, Toast.LENGTH_LONG).show();
                    try {
                        texto = Pgp.decrypt(this, texto);
                        TextView textView = (TextView) findViewById(R.id.textView);
                        textView.setText(texto);
                        textView.setVisibility(View.VISIBLE);
                    } catch (GeneralSecurityException | IOException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case R.id.keys:
                try {
                    KeyPair claves = Pgp.initKeyPair();
                    if (claves != null) {
                        PublicKey pkey = claves.getPublic();
                        String publica = Util.savePublicKey(pkey);
                        Log.d("Publica", publica);
                        PrivateKey skey = claves.getPrivate();
                        String privada = Util.savePrivateKey(skey);
                        Log.d("Privada", privada);
                        Util.guardarPrivada(this, privada);

                        User user = new User("Yo", publica);
                        sqlite.insertUser(user);

                        TextView clave = (TextView) findViewById(R.id.publica);
                        clave.setText(publica);
                        clave.setVisibility(View.VISIBLE);

                        TextView titulo = (TextView) findViewById(R.id.clavePublica);
                        titulo.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.contact:
                Intent intent = new Intent(this, Contacts.class);
                startActivity(intent);
                break;
        }

    }

    private void mostrarUser() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
        builderSingle.setTitle("Selecciona un contacto:");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.select_dialog_singlechoice);
        List<String> usuarios = new SQLite(getApplicationContext()).getAllUsers();
        if (usuarios.size() == 0) {
            Toast.makeText(getApplicationContext(), "No tienes contactos!", Toast.LENGTH_LONG).show();
            return;
        }
        for (String usuario : usuarios) {
            arrayAdapter.add(usuario);
        }

        builderSingle.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                User user = sqlite.selectUser(arrayAdapter.getItem(which));
                if (user != null) {
                    try {
                        texto = Pgp.encrypt(texto, user.getKey()).trim();

                        TextView textView = (TextView) findViewById(R.id.textView);
                        textView.setText(texto);
                        textView.setVisibility(View.VISIBLE);

                        EditText editText = (EditText) findViewById(R.id.editText);
                        editText.setText(texto);
                    } catch (GeneralSecurityException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        builderSingle.show();
    }
}
