package com.elevenpaths.equinox.activity;

/***
 * @author Íñigo Serrano & Miguel Hernández
 */

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.elevenpaths.equinox.R;
import com.elevenpaths.equinox.sqlite.SQLite;

import java.util.List;

public class Contacts extends ListActivity implements AdapterView.OnItemClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.contacts);

        SQLite sqlite = new SQLite(getApplicationContext());

        List<String> listItems = sqlite.getAllUsers();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listItems);
        setListAdapter(adapter);
    }

    @Override
    public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
        String name = ((TextView) view).getText().toString().trim();
    }
}
