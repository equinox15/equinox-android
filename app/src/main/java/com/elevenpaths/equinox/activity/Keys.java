package com.elevenpaths.equinox.activity;

/***
 * @author Íñigo Serrano & Miguel Hernández
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.elevenpaths.equinox.R;
import com.elevenpaths.equinox.pgp.Pgp;
import com.elevenpaths.equinox.sqlite.SQLite;
import com.elevenpaths.equinox.sqlite.User;

import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;

public class Keys extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button generar = (Button) findViewById(R.id.generar);
        generar.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                KeyPair claves;
                try {
                    claves = Pgp.initKeyPair();
                    PublicKey pkey;
                    PrivateKey skey;
                    if (claves != null) {
                        pkey = claves.getPublic();
                        skey = claves.getPrivate();
                        User user = new User("Yo", pkey.toString());
                        SQLite sqlite = new SQLite(getApplicationContext());
                        sqlite.insertUser(user);
                        guardarPrivada(savePrivateKey(skey));
                        TextView clave = (TextView) findViewById(R.id.publica);
                        clave.setText(pkey.toString());
                        TextView titulo = (TextView) findViewById(R.id.clavePublica);
                        titulo.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public static PrivateKey loadPrivateKey(String key64) throws GeneralSecurityException {
        byte[] clear = Base64.decode(key64, Base64.DEFAULT);
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(clear);
        KeyFactory fact = KeyFactory.getInstance("DSA");
        PrivateKey priv = fact.generatePrivate(keySpec);
        Arrays.fill(clear, (byte) 0);
        return priv;
    }


    public static PublicKey loadPublicKey(String stored) throws GeneralSecurityException {
        byte[] data = Base64.decode(stored, Base64.DEFAULT);
        X509EncodedKeySpec spec = new X509EncodedKeySpec(data);
        KeyFactory fact = KeyFactory.getInstance("DSA");
        return fact.generatePublic(spec);
    }

    public static String savePrivateKey(PrivateKey priv) throws GeneralSecurityException {
        KeyFactory fact = KeyFactory.getInstance("DSA");
        PKCS8EncodedKeySpec spec = fact.getKeySpec(priv, PKCS8EncodedKeySpec.class);
        byte[] packed = spec.getEncoded();
        String key64 = Base64.encodeToString(packed, Base64.DEFAULT);

        Arrays.fill(packed, (byte) 0);
        return key64;
    }


    public static String savePublicKey(PublicKey publ) throws GeneralSecurityException {
        KeyFactory fact = KeyFactory.getInstance("DSA");
        X509EncodedKeySpec spec = fact.getKeySpec(publ, X509EncodedKeySpec.class);
        return Base64.encodeToString(spec.getEncoded(), Base64.DEFAULT);
    }

    private void guardarPrivada(String s) throws NoSuchAlgorithmException, InvalidKeySpecException {

        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("private", s);
        editor.apply();
    }

    private String leerPrivada() {
        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        return sharedPref.getString("private", "");
    }
}
